package com.demo.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.hibernate.resource.transaction.spi.TransactionStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.demo.entities.EmployeeEntity;

@Repository
@Transactional
//Neu dung transactional thì ko cần commit nữa vì n tự động commit, rollback cho cta
public class EmployeeDao {
	
	@Autowired
	private SessionFactory sessionFactory;
	//Những method được nằm trong 1 transaction thì chúng ta sẽ gọi 
	//sessionFactory.getCurrentSession() để lấy sesion chứ không gọi sessionFactory.openSession().
	//Lay all
	public List<EmployeeEntity> getAllEmployee(){
		Session session = this.sessionFactory.openSession();
		Query<EmployeeEntity> query = session.createQuery("FROM EmployeeEntity");
//		List<EmployeeEntity> list = query.list();
		return query.list();
	}
	
	//Lay 1tk
	public EmployeeEntity findById2(int id) {
		Session session = this.sessionFactory.openSession();
		return session.get(EmployeeEntity.class, id);
	}
	
	//Lay 1 tk
	public EmployeeEntity findById(int id) {
		Session session = this.sessionFactory.openSession();
		Query<EmployeeEntity> query = (Query<EmployeeEntity>) session.createQuery("from EmployeeEntity where id =?1").setParameter(1, id);
		return query.getSingleResult();
	}
	
	//Them san pham
	public EmployeeEntity saveEmp(EmployeeEntity employeeEntity) {
		//Dung getCurrentSession() thi loi~
		//Session session = this.sessionFactory.openSession();
		Session session = sessionFactory.openSession();
		try {
			session.beginTransaction();
			session.save(employeeEntity);
			session.getTransaction().commit();
			session.close();
			//return true;
			
		} catch (Exception e) {
			e.printStackTrace();
			session.getTransaction().rollback();
		
		}
		finally
		{
			session.close();
		}
		return employeeEntity;
	}
	
	//Sua san pham
	public EmployeeEntity editEmp(EmployeeEntity employeeEntity) {
		//Dung getCurrentSession() thi loi~
		//Session session = this.sessionFactory.openSession();
		Session session = sessionFactory.openSession();
		try {
			session.beginTransaction();
//			Query<EmployeeEntity> q = session.createQuery("Update EmployeeEntity set address =?1, name =?2 where id=?3");
//			q.setParameter(1, employeeEntity.getAddress());
//			q.setParameter(2, employeeEntity.getName());
//			q.setParameter(3, id);
			session.update(employeeEntity);
			session.getTransaction().commit();
			session.close();
			//return true;
			
		} catch (Exception e) {
			e.printStackTrace();
			session.getTransaction().rollback();
		
		}
		finally
		{
			session.close();
		}
		return null;
	}
	
	public EmployeeEntity editEmployee(EmployeeEntity employeeEntity,int id) {
		//Dung getCurrentSession() thi loi~
		//Session session = this.sessionFactory.openSession();
		Session session = sessionFactory.openSession();
		try {
			session.beginTransaction();
		//	Transaction tx = session.beginTransaction();
		//	tx.commit();
			Query<EmployeeEntity> q = session.createQuery("Update EmployeeEntity set address =?1, name =?2 where id=?3");
			q.setParameter(1, employeeEntity.getAddress());
			q.setParameter(2, employeeEntity.getName());
			q.setParameter(3, id);
			EmployeeEntity e = q.getSingleResult();
			session.update(e);
			session.getTransaction().commit();
			session.close();
			//return true;
			
		} catch (Exception e) {
			e.printStackTrace();
			session.getTransaction().rollback();
		}
		finally
		{
			session.close();
		}
		return null;
	}

	public void delete(int id) {
		Session session = this.sessionFactory.openSession();
		session.beginTransaction();
		EmployeeEntity emp = (EmployeeEntity) session.createQuery("FROM EmployeeEntity e where e.id=?1").setParameter(1, id).getSingleResult();
		session.delete(emp);
		session.getTransaction().commit();
		session.close();
	}
	
	public List<EmployeeEntity> getLisyEmployeeByPagination(int start,int end){
		Session session = sessionFactory.openSession();
		Query<EmployeeEntity> query = session.createQuery("From EmployeeEntity");
		query.setFirstResult(start * end);
		query.setMaxResults(end);
		List<EmployeeEntity> list = query.list();
		return list;
	}
//	public void save(EmployeeEntity employeeEntity) {
//		Session session = this.sessionFactory.getCurrentSession();
//		Transaction tx = session.beginTransaction();
//		session.save(employeeEntity);
//		tx.commit();
//		session.close();
//	}
}


//Truy vấn phân trang
// Session session = sessionFactory.openSession()
// string hql ="FROM student"
// Query query = session.createQuery(hql);
// query.setFirstResult(5) Chi ra vi tri bat dau truy van
// query.setMaxResult(10) Chi ra so luong toi da truy van dc
// List<Major> list = query.list()




