package com.demo.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.demo.dao.EmployeeDao;
import com.demo.entities.EmployeeEntity;

@RestController
public class EmployeeAPI {
	@Autowired
	private EmployeeDao employeeDao;
	//Lay het
	@GetMapping("/api/employee")
	public List<EmployeeEntity> getAll(){
		return employeeDao.getAllEmployee();
	}
	
	//Lay 1
	@GetMapping("/api/employee/{id}")
	public EmployeeEntity getOne(@PathVariable int id) {
		return employeeDao.findById(id);
	}
	@GetMapping("/api/employeePage")
	public List<EmployeeEntity> getPage(@RequestParam(defaultValue = "1") int start) {
		return employeeDao.getLisyEmployeeByPagination(start-1, 2);
	}
	
	@PostMapping("/api/employee")
	public EmployeeEntity save(@RequestBody EmployeeEntity employeeEntity) {
		return employeeDao.saveEmp(employeeEntity);
	}
	@PutMapping("/api/employee")
	public EmployeeEntity edit(@RequestBody EmployeeEntity employeeEntity) {
		return employeeDao.editEmp(employeeEntity);
	}
	@PutMapping("/api/employee/{id}")
	public EmployeeEntity edit(@RequestBody EmployeeEntity employeeEntity,@PathVariable int id) {
		return employeeDao.editEmployee(employeeEntity,id);
	}
	@DeleteMapping("/api/employee/{id}")
	public void delete(@PathVariable int id) {
		employeeDao.delete(id);
	}
}
