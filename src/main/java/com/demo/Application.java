package com.demo;

import java.io.IOException;
import java.util.Properties;

import javax.sql.DataSource;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;


@SpringBootApplication
//Mục đích trong ứng dụng này chúng ta sẽ sử dụng Hibernate, vì vậy chúng ta cần vô hiệu hóa các cấu hình tự động nói trên của Spring Boot.
@EnableAutoConfiguration(exclude = {DataSourceAutoConfiguration.class,DataSourceTransactionManagerAutoConfiguration.class,HibernateJpaAutoConfiguration.class})
public class Application {
	@Autowired
	private Environment env;
	
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
	
	@Bean(name = "dataSource")
	public DataSource getDataSource() {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName(env.getProperty("spring.datasource.driver-class-name"));
		dataSource.setUrl(env.getProperty("spring.datasource.url"));
		dataSource.setUsername(env.getProperty("spring.datasource.username"));
		dataSource.setPassword(env.getProperty("spring.datasource.password"));
		return dataSource;
	}
	
	@Bean(name ="sessionFactory")
	public SessionFactory getSessionFactory() {
		LocalSessionFactoryBean factoryBean = new LocalSessionFactoryBean();
		factoryBean.setDataSource(getDataSource());
		factoryBean.setPackagesToScan("com.demo.entities");
		Properties prop = new Properties();
		prop.put("hibernate.show_sql", env.getProperty("spring.jpa.show-sql"));
		prop.put("hibernate.dialect", env.getProperty("spring.jpa.properties.hibernate.dialect"));
		prop.put("hibernate.current_session_context_class", env.getProperty("spring.jpa.properties.hibernate.current_session_context_class"));
		factoryBean.setHibernateProperties(prop);
		try {
			factoryBean.afterPropertiesSet();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return factoryBean.getObject();
	}
//	@Bean(name = "sessionFactory")
//	public SessionFactory getSessionFactory()
//	{
//		LocalSessionFactoryBean factoryBean=new LocalSessionFactoryBean();
//		factoryBean.setDataSource(getDataSource());
//		factoryBean.setPackagesToScan("com.demo.entities"); 
//		Properties p=new Properties();
//		p.put("hibernate.show_sql", env.getProperty("spring.jpa.show-sql"));
//		p.put("hibernate.dialect", env.getProperty("spring.jpa.properties.hibernate.dialect"));
//		p.put("hibernate.current_session_context_class",env.getProperty("spring.jpa.properties.hibernate.current_session_context_class") );
//		
//		factoryBean.setHibernateProperties(p);
//		try {
//			factoryBean.afterPropertiesSet();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		return factoryBean.getObject();
//		
//		
//	}
	
	
    @Bean(name = "transactionManager")
    public HibernateTransactionManager getTransactionManager(SessionFactory sessionFactory) {
        HibernateTransactionManager transactionManager = new HibernateTransactionManager(sessionFactory);
        return transactionManager;
    }
}
